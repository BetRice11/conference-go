from common.json import ModelEncoder
from events.models import Conference, Location
from attendees.models import Attendee
from presentations.models import Presentation




class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name"
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]


class AttendeesListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name"
    ]

class AttendeesDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",

    ]
    encoders = {
        "conference": ConferenceDetailEncoder(),
    }


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "status"
    ]
    def get_extra_data(self, o):
        return {"status": {
                    "name": o.status.name}
        }

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }
